<?php 

namespace App\Service;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

//--------------------------------------------------------

class RollAbilities
{	
	public $abilities = [
					'strength'	   => 0,
					'dexterity'	   => 0,
					'constitution' => 0,
					'inteligence'  => 0,
					'wisdom'       => 0,
					'charisma'	   => 0
				];	
	

	public function rollAttribute($ddCharacter)
	{
		foreach ($this->abilities as $key => $value) {
					$this->abilities[$key] = rand(3,13);
				}	
						
		return $this->abilities; 
	

	}
	
}// end of class
