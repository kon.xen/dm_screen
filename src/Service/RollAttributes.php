<?php 

namespace App\Service;
//--------------------------------------------------------


/**
 * 
 */
class RollAttributes
{	
	public $attribute;

	public $check_class = [
		"bard"        => true,
		"cleric"      => true,
		"druid"       => true,
		"fighter"     => true,
		"illusionist" => true,
		"mage"        => true,
		"palladin"    => true,
		"ranger"      => true,
		"thief"       => true
		];

	public $abilities = [
					'strength'	   => '',
					'dexterity'	   =>'',
					'constitution' =>'',
					'inteligence'  =>'',
					'wisdom'       =>'',
					'charisma'	   =>''
				];
			



	public function threeDSix()
	{
		return rand(3,18);
	}

	public function rollAttribute($charId)
	{
		foreach ($abilities as $key => $value) {
					$abilities[$key] = rand(3,13);
				}	
						
			$ddCharacter->setCharStrength($abilities['strength']);
			$ddCharacter->setCharDexterity($abilities['dexterity']);
			$ddCharacter->setCharConstitution($abilities['constitution']);
			$ddCharacter->setCharInteligence($abilities['inteligence']);
			$ddCharacter->setCharWisdom($abilities['wisdom']);
			$ddCharacter->setCharCharisma($abilities['charisma']); 

		return new RedirectResponse($this->router->generate('char_output'));
	}

	
	
	public function adjust($value='')
	{
		# code...
	}

	public function raceToclassConficts($char_race)
	{
		if ($char_race = 'human') {

			$this->check_class = [
				"bard"   	  => true,
	 		    "cleric" 	  => true,
				"druid" 	  => true,
	 		 	"fighter" 	  => true,
				"illusionist" => true,
				"mage" 		  => true,
	 			"palladin"    => true,
				"ranger" 	  => true,
				"thief" 	  => true, 	
			];
		}

		if ($char_race = 'Dwarf') {
			
			$this->check_class = [
				'bard'        => false,
				'cleric'	  => true,
				'druid'       => false,
				'fighter'     => true,
				'illusionist' => false,
				'mage'        => false,
				'palladin'    => false,
				'ranger'      => false,
				'thief'       => true,
			];
		}

		if ($char_race = 'elf') {
			
			$this->check_class = [
				'bard'        => false,
				'cleric'      => true,
				'druid'       => false,
				'fighter'     => true,
				'illusionist' => false,
				'mage'        => true,
				'palladin'    => false,
				'ranger'      => true,
				'thief'       => true
			];
		}

		if ($char_race = 'gnome') {
			
			$this->check_class = [
				'bard'        => false,
				'cleric'      => true,
				'druid'       => false,
				'fighter'     => true,
				'illusionist' => true,
				'mage'        => false,
				'palladin'    => false,
				'ranger'      => false,
				'thief'       => true,
			];
		}

		if ($char_race = 'gnome') {
			
			$this->check_class = [
				'bard'        => true,
				'cleric'      => true,
				'druid'       => true,
				'fighter'     => true,
				'illusionist' => false,
				'mage'        => true,
				'palladin'    => false,
				'ranger'      => true,
				'thief'       => true,
			];
		}

	}
}// end of class
