<?php

namespace App\Repository;

use App\Entity\DDCharacter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method DDCharacter|null find($id, $lockMode = null, $lockVersion = null)
 * @method DDCharacter|null findOneBy(array $criteria, array $orderBy = null)
 * @method DDCharacter[]    findAll()
 * @method DDCharacter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DDCharacterRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, DDCharacter::class);
    }

//    /**
//     * @return DDCharacter[] Returns an array of DDCharacter objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DDCharacter
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
