<?php 

//------------------------------------------------------------------

namespace App\Form;

use App\Entity\DDCharacter;
use Symfony\Component\Form\AbstractType;
//use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;

//-------------------------------------------------------------------

class CharacterEdit extends AbstractType
{
	// overriding two methods from the parent class..

	public function buildForm(FormbuilderInterface $builder, array $options)
	{
		$builder->add('charName', TextType::class, ['label' => 'Name'])
				//->get('charImage',FileType::class, ['label' => 'Character Image'])
				//->add('charImage',FileType::class, ['label' => 'Character Image'])
				->add('charRace', ChoiceType::class,
					[ 'choices' => [
									'Human'   		  => 'Human',
					                'Elf' 	   		  => 'Elf',
					                'Half-Elf' 		  => 'Half-Elf', 
					                'Halfling' 		  => 'Halfling',
					                'Dwarf'    		  => 'Dwarf',
					                'Gnome'   		  => 'Gnome' ]
						        ]
						    )
				->add('charClass', ChoiceType::class,
					[ 'choices' =>  [
									'Rogue' =>[
										'Bard'        => 'Bard',
										'Thief'       => 'Thief'
										],									
									'Warior' =>[
										'Fighter' 	  => 'Fighter',
										'Ranger'      => 'Ranger',
										'Palladin'    => 'Palladin'
										],
									'Wizard' =>[
										'Mage'        => 'Mage',
										'Illusionist' => 'Illusionist',
										'Necromancer' => 'Necromancer'
										],
					                'Priest' =>[
					                	'Cleric' 	  => 'Cleric',
					                	'Druid' 	  => 'Druid'
					                	]
	            				    ]
	            	]
	            )
				->add('charAlignment', ChoiceType::class,
					[ 'choices' => [
									'Lawful Good'     => 'lawful good',
					                'Lawful Neutral'  => 'lawful neutral',
					                'Lawful Evil' 	  => 'lawful evil', 
					                'Neutral Good'	  => 'neutral good',
					                'True Neutral'    => 'true neutral',
					                'Neutral Evil'    => 'neutral evil',
					                'Chaotic Good'    => 'chaotic good',
					                'Chaotic Neutral' => 'chaotic neutral',
					                'Chaotic Evil' 	  => 'chaotic evil' ]
					            ])
				->add('charLevel',	 	  TextType::class, ['label' => 'Level'])
				->add('charExperience',   TextType::class, ['label' => 'Experience'])
				->add('charMagic', 	      TextType::class, ['label' => 'Magic'])
				->add('charStrength', 	  TextType::class, ['label' => 'Strength'])
				->add('charDexterity', 	  TextType::class, ['label' => 'Dexterity'])
				->add('charInteligence',  TextType::class, ['label' => 'Inteligence'])
				->add('charConstitution', TextType::class, ['label' => 'Constitution'])
				->add('charWisdom', 	  TextType::class, ['label' => 'Wisdom'])
				->add('charCharisma', 	  TextType::class, ['label' => 'Charisma'])
				->add('charHistory', TextareaType::class,
					['required'   => false, 'empty_data' =>''],
					['label'=> 'Character background']
				);
				// ->add('charType', CheckboxType::class, [
				// 		'mapped'	  => false,
				// 		'constraints' => new IsTrue(),
				// 		'label' 	  => 'Non Player Character'
				// ])
								
				//->add('Save', SubmitType::class);

		// $builder->get('charImage')
	 //            ->addModelTransformer(new CallbackTransformer(
	 //            	function ($imageAsArray) {
	 //            	// transform the array to a string
	 //                    return implode(",", $imageAsArray);
	 //                },
	 //                function ($imageAsString) {
	 //                    // transform the string back to an array
	 //                    return explode(",", $imageAsString);
	 //                }
	 //            ));
	
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => DDCharacter::class            
		 ]              
		);
	}


}