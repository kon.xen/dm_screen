<?php 

//------------------------------------------------------------------

namespace App\Form;

use App\Entity\DDCharacter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;

//-------------------------------------------------------------------

class CharacterType extends AbstractType
{
	// overriding two methods from the parent class..

	public function buildForm(FormbuilderInterface $builder, array $options)
	{
		$builder->add('charName', TextType::class, ['label' => 'Name'])
				->add('charImage',FileType::class, 
					['label' => 'Character Image'])
				->add('charRace', ChoiceType::class,
					[ 'choices' => [
									'Human'   		  => 'human',
					                'Elf' 	   		  => 'elf',
					                'Half-Elf' 		  => 'half-elf', 
					                'Halfling' 		  => 'halfling',
					                'Dwarf'    		  => 'dwarf',
					                'Gnome'   		  => 'gnome' 
					            ]
						        ]
						    )
				->add('charClass', ChoiceType::class,
					[ 'choices' =>  [
									'Rogue' =>[
										'Bard'        => 'bard',
										'Thief'       => 'thief'
										],									
									'Warior' =>[
										'Fighter' 	  => 'fighter',
										'Ranger'      => 'ranger',
										'Palladin'    => 'palladin'
										],
									'Wizard' =>[
										'Mage'        => 'mage',
										'Illusionist' => 'illusionist',
										'Necromancer' => 'necromancer'
										],
					                'Priest' =>[
					                	'Cleric' 	  => 'cleric',
					                	'Druid' 	  => 'druid'
					                	]
	            				    ]
	            	]
	            )
				->add('charAlignment', ChoiceType::class,
					[ 'choices' => [
									'Lawful Good'     => 'lawful good',
					                'Lawful Neutral'  => 'lawful neutral',
					                'Lawful Evil' 	  => 'lawful evil', 
					                'Neutral Good'	  => 'neutral good',
					                'True Neutral'    => 'true neutral',
					                'Neutral Evil'    => 'neutral evil',
					                'Chaotic Good'    => 'chaotic good',
					                'Chaotic Neutral' => 'chaotic neutral',
					                'Chaotic Evil' 	  => 'chaotic evil' ]
					            ])
				->add('charHistory', TextareaType::class,
					['required'   => false, 'empty_data' =>''],
					['label'=> 'Character background']
				);
				// ->add('charType', CheckboxType::class, [
				// 		'mapped'	  => false,
				// 		'constraints' => new IsTrue(),
				// 		'label' 	  => 'Non Player Character'
				// ])
				//->add('Create', SubmitType::class);

	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => DDCharacter::class            
		 ]              
		);
	}


}