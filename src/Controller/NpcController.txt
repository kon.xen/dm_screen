<?php 
 
 namespace App\Controller;
 use App\Entity\DDCharacter;
 use App\Form\CharacterType;
 use App\Repository\DDCharacterRepository;
 use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
 use Symfony\Component\HttpFoundation\Request;
 use Symfony\Component\Routing\Annotation\Route;
 use Symfony\Component\Form\FormFactoryInterface;
// use Symfony\Component\HttpFoundation\RedirectResponse;
 use Symfony\Component\HttpFoundation\Response;
// use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
// use Symfony\Component\HttpFoundation\Session\Flash\SessionInterface;
// use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
// use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
// use Symfony\Component\Routing\RouterInterface;
//------------------------------------------------------------------

/**
 * @Route("/character")
 */
class NpcController extends CharacterController
{
	/**
	 * @var("\Twig_Environment")
	 */
	private $twig;

	/**
	 * @var NpcRepository
	 */
	private $npcRepository;

	public function __Construct(
								\Twig_Environment $twig,
								 DDCharacterRepository $NpcRepository,
								 FormFactoryInterface $formFactory
								)
	{

		$this->twig = $twig;
		$this->NpcRepository = $npcRepository;
		$this->formFactory = $formFactory;
	}

	/**
	 * @Route("/", name="char_index")
	 */
	public function index()
	{
		$html = $this->twig->render(
			'character/index.html.twig', 
			['npCharacters' => $this->npcRepository->findAll()]
		);

		return new Response($html);

	}

	/**
	 * @Route("/add", name="char_new")
	 */
	// public function add(Request $request)
	// {
	// 	$ddCharacter = new DDCharacter;
	// 	$ddCharacter->setCharTime(new \DateTime());

	// 	// to use the characterType form we inject FormFactory Interface\

	// 	// use form instance to render the form
	// 	$form = $this->formFactory->create( CharacterType::class, $ddCharacter );
	// 	$form->handleRequest($request);

	// 	if ($form->isSubmitted() && $form->isValid()) {

	// 	}

	// 	return new Response(
	// 		$this->twig->render(
	// 			'character/char_new.html.twig',
	// 			['form'=> $form->createView()]
	// 			)
	// 	);
	// }

	/**
	 * @Route("/{charId}", name="char_output")
	 */
	public function show($npcId)
	{
		$NpCharacter = $this->NpcRepository->find($npcId);
		return new Response(
			$this->twig->render('character/char_output.html.twig', ['character' => $character]));	
	}

	
}
