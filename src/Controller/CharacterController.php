<?php 
//------------------------------------------------------------------ 
 namespace App\Controller;

 use App\Entity\DDCharacter;
 use App\Form\CharacterType;
 use App\Form\CharacterEdit;
 use App\Service\RollAbilities;
 use App\Repository\DDCharacterRepository;
 use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
 use Symfony\Component\HttpFoundation\Request;
 use Symfony\Component\Routing\Annotation\Route;
 use Symfony\Component\Form\FormFactoryInterface;
 use Doctrine\ORM\EntityManagerInterface;
 use Symfony\Component\HttpFoundation\RedirectResponse;
 use Symfony\Component\HttpFoundation\Response;
 use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
 use Symfony\Component\HttpFoundation\Session\Flash\SessionInterface;
 use Symfony\Component\HttpFoundation\File\UploadedFile;
// use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
// use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
 use Symfony\Component\Routing\RouterInterface;
//------------------------------------------------------------------

/**
 * @Route("/character")
 */
class CharacterController extends AbstractController
{
	/**
	 * @var("\Twig_Environment")
	 */
	private $twig;

	/**
	 * @var DDCharacterRepository
	 */
	private $CharacterRepository;

	public function __Construct(
								\Twig_Environment $twig,
								 DDCharacterRepository $ddCharacterRepository,
								 FormFactoryInterface $formFactory,
								 EntityManagerInterface $entityManager,
								 RouterInterface $router,								 
								 FlashBagInterface $flashBag,
								 RollAbilities $roll							
								)
	{
		$this->twig 		  	     = $twig;
		$this->ddCharacterRepository = $ddCharacterRepository;
		$this->formFactory 		     = $formFactory;
		$this->entityManager 	     = $entityManager;
		$this->router 			     = $router;
		$this->flashBag		         = $flashBag;
		$this->roll 			     = $roll;
	}


	/**
	 * @Route("/", name="char_index")
	 */
	public function index()
	{
		$html = $this->twig->render(
			'character/index.html.twig', 
			['characters' => $this->ddCharacterRepository->findall([], ['charTime' => 'DESC' ])]
		);

		return new Response($html);
	}

	
	/**
	 * @Route("/add", name="char_new")
	 */
	public function add(Request $request)
	{
		$ddCharacter = new DDCharacter;
		$ddCharacter->setCharTime(new \DateTime());

		// to use the characterType form we inject FormFactory Interface\

		// use form instance to render the form
		$form = $this->formFactory->create( CharacterType::class, $ddCharacter );
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {

			// Sort out the image first
			$this->saveImage($form,$ddCharacter);

			//Roll abilities & fill the fields
			$this->roll->rollAttribute($ddCharacter);
									
			$ddCharacter->setCharStrength($this->roll->abilities['strength']);
			$ddCharacter->setCharDexterity($this->roll->abilities['dexterity']);
			$ddCharacter->setCharConstitution($this->roll->abilities['constitution']);
			$ddCharacter->setCharInteligence($this->roll->abilities['inteligence']);
			$ddCharacter->setCharWisdom($this->roll->abilities['wisdom']);
			$ddCharacter->setCharCharisma($this->roll->abilities['charisma']);
			
			// Set other jazz
			$ddCharacter->setCharLevel(1);
			
			$ddCharacter->setCharMagic(100);
			$ddCharacter->setCharExperience(0);
			$ddCharacter->setCharHitPoints(100);
			
			//Persist the Database
			$this->entityManager->persist($ddCharacter);
			$this->entityManager->flush();

			return new RedirectResponse($this->router->generate('char_index'));
		}


		return new Response(
			$this->twig->render(
				'character/char_new.html.twig',
				['form'=> $form->createView()]
				)
		);
	}


	/**
	 * @Route("/edit/{charId}", name="char_edit")
	 *
	 */
	public function edit(DDCharacter $ddCharacter, Request $request )
	{
		//$ddCharacter = $this->ddCharacterRepository->findBy(['charId' => $charId]);
				

		// use form instance to render the form
		$form = $this->formFactory->create( CharacterEdit::class, $ddCharacter );
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {

			// Sort out the image first
			//$this->saveImage($form,$ddCharacter);
	
			$this->entityManager->flush();

			return new RedirectResponse(
				$this->router->generate('char_index')
			);
		}


		return new Response(
			$this->twig->render(
				'character/char_edit.html.twig',
				['form'=> $form->createView()]
			)
		);
	}


	/**
	 * @Route("/delete/{charId}", name="char_delete")
	 */
	public function delete(DDCharacter $ddCharacter)
	{
		$this->entityManager->remove($ddCharacter);
		$this->entityManager->flush();

		$this->flashBag->add('notice', 'Character deleted');

		return new RedirectResponse(
			$this->router->generate('char_index')
		);

	}


	/**
	 * @Route("/reroll/{charId}", name="char_reroll")
	 */
	public function rollAttribute(DDCharacter $ddCharacter)
	{ 
		$this->roll->rollAttribute($ddCharacter);

		$ddCharacter->setCharStrength($this->roll->abilities['strength']);
		$ddCharacter->setCharDexterity($this->roll->abilities['dexterity']);
		$ddCharacter->setCharConstitution($this->roll->abilities['constitution']);
		$ddCharacter->setCharInteligence($this->roll->abilities['inteligence']);
		$ddCharacter->setCharWisdom($this->roll->abilities['wisdom']);
		$ddCharacter->setCharCharisma($this->roll->abilities['charisma']);
		
		$this->entityManager->flush();
		
		return new Response(
		$this->twig->render('character/char_output.html.twig', ['character' => $ddCharacter]));	
	}


	/**
	 * @Route("/{charId}", name="char_output")
	 */
	public function show(DDCharacter $ddCharacter)
	{
		//$character = $this->CharacterRepository->find($charId);
		return new Response(
			$this->twig->render('character/char_output.html.twig', ['character' => $ddCharacter]));	
	}


	/**
	 * @var("ddCharacter")
	 */
	public function saveImage($form,$ddCharacter)
	{
		if (!$form['charImage']) {

			$filename = $ddCharacter->getCharImage();

			$ddCharacter->setCharImage($filename);
		} else {

		$file = $form['charImage']->getData();

			$extension = $file->guessExtension();
			
			if (!$extension) {
				$extension = 'bin';
			}

			$filename = rand(1,99999) . '.' . $extension;
			$file->move('characterimages', $filename);

			$ddCharacter->setCharImage($filename);
		}
	}
}
