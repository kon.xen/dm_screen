<?php 
 
 namespace App\Controller;

 use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
 use Symfony\Component\HttpFoundation\Request;
 use Symfony\Component\Routing\Annotation\Route;
//------------------------------------------------------------------

class Registration extends AbstractController
{
	/**
	 * @Route("/registration", name="dm_registration")
	 */
	public function showCharacter(Request $request)
	{
		return $this->render('register/registration.html.twig');
	}
}
