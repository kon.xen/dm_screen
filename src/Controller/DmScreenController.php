<?php 
 
 namespace App\Controller;

 use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
 use Symfony\Component\HttpFoundation\Request;
 use Symfony\Component\Routing\Annotation\Route;
//------------------------------------------------------------------

class DmScreenController extends AbstractController
{
	/**
	 * @Route("/", name="initial")
	 */
	public function index(Request $request)
	{
		return $this->render('index.html.twig');
	}

	/**
	 * @Route("/dmhome", name="vallet")
	 */
		public function dmHome(Request $request)
	{
		return $this->render('dmaster/dmhome.html.twig');
	}

	/**
	 * @Route("/dmplay", name="dm_play")
	 */
		public function dmPlay(Request $request)
	{
		return $this->render('dmaster/play_screen.html.twig');
	}
}
