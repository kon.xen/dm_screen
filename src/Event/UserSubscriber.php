<?php

namespace App\Event;

//use App\Entity\UserPreferences;
use App\Mailer\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

//------------------------------------------------------------------

class UserSubscriber implements EventSubscriberInterface
{
    /**
    *@var Mailer
    **/
    private $mailer;

    /**
    *@var EntityManager
    **/
    private $entityManager;


    public function __construct(
        Mailer $mailer, 
        EntityManagerInterface $entityManager
    ){
        $this->mailer   = $mailer;
        $this->twig     = $twig;
    }

    public static function getSubscribedEvents()
    {
    	return  [
    		UserRegisterEvent::NAME => 'onUserRegister'
    	];
    }

    public function onUserRegister(UserRegisterEvent $event)
    {
        //$user = $event->getRegisteredUser();
        //$user->setPreferences($preferences);

        //$this->entityManager->flush();

        $this->mailer->sendConfirmationEmail($event->getRegisteredUser());   	
        
    }
}