<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200114151220 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE notification (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, seen TINYINT(1) NOT NULL, INDEX IDX_BF5476CAA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CAA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('DROP TABLE `character`');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `character` (char_id INT AUTO_INCREMENT NOT NULL, char_time DATETIME NOT NULL, char_name VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, char_race VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, char_class VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, char_level INT NOT NULL, char_alignment VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, char_hit_points INT NOT NULL, char_experience INT NOT NULL, char_magic INT NOT NULL, char_history VARCHAR(280) NOT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(char_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE notification');
    }
}
