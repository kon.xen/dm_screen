<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190703113433 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_preferences (id INT AUTO_INCREMENT NOT NULL, locale VARCHAR(8) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE `character`');
        $this->addSql('DROP TABLE elephant');
        $this->addSql('ALTER TABLE user ADD preferences_id INT DEFAULT NULL, ADD roles LONGTEXT NOT NULL COMMENT \'(DC2Type:simple_array)\', ADD confirmation_token VARCHAR(30) DEFAULT NULL, ADD enabled TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6497CCD6FB7 FOREIGN KEY (preferences_id) REFERENCES user_preferences (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D6497CCD6FB7 ON user (preferences_id)');
        $this->addSql('ALTER TABLE ddcharacter ADD user_id INT NOT NULL, ADD npc VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE ddcharacter ADD CONSTRAINT FK_26F554C6A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_26F554C6A76ED395 ON ddcharacter (user_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6497CCD6FB7');
        $this->addSql('CREATE TABLE `character` (char_id INT AUTO_INCREMENT NOT NULL, char_time DATETIME NOT NULL, char_name VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, char_race VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, char_class VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, char_level INT NOT NULL, char_alignment VARCHAR(50) NOT NULL COLLATE utf8mb4_unicode_ci, char_hit_points INT NOT NULL, char_experience INT NOT NULL, char_magic INT NOT NULL, char_history VARCHAR(280) NOT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(char_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE elephant (id INT AUTO_INCREMENT NOT NULL, feet VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, trunk VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE user_preferences');
        $this->addSql('ALTER TABLE ddcharacter DROP FOREIGN KEY FK_26F554C6A76ED395');
        $this->addSql('DROP INDEX IDX_26F554C6A76ED395 ON ddcharacter');
        $this->addSql('ALTER TABLE ddcharacter DROP user_id, DROP npc');
        $this->addSql('DROP INDEX UNIQ_8D93D6497CCD6FB7 ON user');
        $this->addSql('ALTER TABLE user DROP preferences_id, DROP roles, DROP confirmation_token, DROP enabled');
    }
}
