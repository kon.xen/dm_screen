<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190703104322 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user_preferences (id INT AUTO_INCREMENT NOT NULL, locale VARCHAR(8) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, preferences_id INT DEFAULT NULL, username VARCHAR(50) NOT NULL, password VARCHAR(255) NOT NULL, email VARCHAR(250) NOT NULL, fullname VARCHAR(50) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:simple_array)\', confirmation_token VARCHAR(30) DEFAULT NULL, enabled TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), UNIQUE INDEX UNIQ_8D93D6497CCD6FB7 (preferences_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ddcharacter (char_id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, npc VARCHAR(255) NOT NULL, char_time DATETIME NOT NULL, char_name VARCHAR(50) NOT NULL, char_race VARCHAR(50) NOT NULL, char_class VARCHAR(50) NOT NULL, char_level INT NOT NULL, char_alignment VARCHAR(50) NOT NULL, char_hit_points INT NOT NULL, char_experience INT NOT NULL, char_magic INT NOT NULL, char_history TEXT NOT NULL, char_image VARCHAR(255) NOT NULL, char_strength INT NOT NULL, char_dexterity INT NOT NULL, char_constitution INT NOT NULL, char_inteligence INT NOT NULL, char_wisdom INT NOT NULL, char_charisma INT NOT NULL, INDEX IDX_26F554C6A76ED395 (user_id), PRIMARY KEY(char_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6497CCD6FB7 FOREIGN KEY (preferences_id) REFERENCES user_preferences (id)');
        $this->addSql('ALTER TABLE ddcharacter ADD CONSTRAINT FK_26F554C6A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6497CCD6FB7');
        $this->addSql('ALTER TABLE ddcharacter DROP FOREIGN KEY FK_26F554C6A76ED395');
        $this->addSql('DROP TABLE user_preferences');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE ddcharacter');
    }
}
