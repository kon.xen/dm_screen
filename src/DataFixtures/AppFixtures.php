<?php 
//------------------------------------------------------------------------
namespace App\DataFixtures;

use App\Entity\DDCharacter;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
//------------------------------------------------------------------------

/**
 * 
 */
class AppFixtures extends Fixture
{
	/**
	 * @var passwordEncoder;
	 */
	private $passwordEncoder;

	public function __construct(UserPasswordEncoderInterface $passwordEncoder)
	{
		$this->passwordEncoder = $passwordEncoder;
	}

	public function load(ObjectManager $manager)
	{
		$this->loadUsers($manager);		
	}

	private function loadUsers(ObjectManager $manager)
	{
		$user = new User;
		$user->setUsername('doctor');
		$user->setFullname('The Doctor');
		$user->setEmail('thedoctor@galifrey.gal');
		$user->setPassword($this->passwordEncoder->encodePassword($user,  'tardis'));
		
		$manager->persist($user);
		$manager->flush();
	}
		
	// private function loadCharacters(ObjectManager $manager)
	// {
	// 	for ($i=0; $i < 10; $i++) { 
	// 		$ddCharacter = new DDCharacter;
	// 		$ddCharacter->setCharTime(new \DateTime('2018-09-15'));
	// 		$ddCharacter->setCharName('Adventurer');
	// 		$ddCharacter->setCharName('Adventurer');
	// 		$ddCharacter->setCharRace('Race');
	// 		$ddCharacter->setCharClass('Some Class');
	// 		$ddCharacter->setCharLevel(rand(1,50));
	// 		$ddCharacter->setCharAlignment('Neutral');
	// 		$ddCharacter->setCharHitPoints(rand(5,20));
	// 		$ddCharacter->setCharExperiencePoints(rand(1,1000));
	// 		$ddCharacter->setCharMagic(rand(1,100));
	// 		$ddCharacter->setCharHistory('a rich and interesting one');
	// 		$manager->persist($ddCharacter);
	// 	}

	// 	$manager->flush();
	// }
}


