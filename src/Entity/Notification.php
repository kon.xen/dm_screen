<?php 

namespace App\Entity;
//--------------------------------------------
use Doctrine\ORM\Mapping as ORM;
//--------------------------------------------

/**
 * @ORM\Entity(repositoryClass="App\Repository\NotificationRepository")
 */
 class Notification 
 {
	/**
	* @ORM\Id()
	* @ORM\GeneratedValue()
	* @ORM\Column(type="integer")
	*/
	private $id;

	/**
	* @ORM\ManyToOne(targetEntity="App\Entity\User")
	*/
	private $user;

	/**
	* @ORM\Column(type="boolean")
	*/
	private $seen;

	public function __construct()
	{
		$this->seen = false;	 
	}
 } 
