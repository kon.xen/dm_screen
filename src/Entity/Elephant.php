<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ElephantRepository")
 */
class Elephant
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $feet;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $trunk;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFeet(): ?string
    {
        return $this->feet;
    }

    public function setFeet(?string $feet): self
    {
        $this->feet = $feet;

        return $this;
    }

    public function getTrunk(): ?string
    {
        return $this->trunk;
    }

    public function setTrunk(string $trunk): self
    {
        $this->trunk = $trunk;

        return $this;
    }
}
