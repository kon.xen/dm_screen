<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ORM\Entity(repositoryClass="App\Repository\DDCharacterRepository")
* @ORM\Table()
*/
class DDCharacter
{
/**
 * @ORM\Id()
 * @ORM\GeneratedValue()
 * @ORM\Column(type="integer")
 */
private $charId;

/**
 * @ORM\Column(type="datetime")
 */
private $charTime;

// Annotate and activate later... 
// private $user;

 /**
 * @ORM\Column(type="string", length=50)
 * @Assert\NotBlank()
 * @Assert\Length(max=50, maxMessage="Something smaller please") * 
 */
private $charName;

 /**
 * @ORM\Column(type="string", length=50)
 */
private $charRace;

 /**
 * @ORM\Column(type="string", length=50)
 *
 */
private $charClass;

 /**
 * @ORM\Column(type="integer", length=10)
 * 
 */
private $charLevel;

 /**
 * @ORM\Column(type="string", length=50)
 * 
 */
private $charAlignment;

 /**
 * @ORM\Column(type="integer", length=10)
 * 
 */
private $charHitPoints;

/**
 * @ORM\Column(type="integer", length=50)
 * 
 */
private $charExperience;

/**
 * @ORM\Column(type="integer", length=10)
 * 
 */
private $charMagic;  

/**
 * @ORM\Column(type="text", length=280)
 */
private $charHistory;

/**
 * @ORM\Column(type="string")
 * Assert\File(mimeTypes={ "jpg,png,bmp" })
 */
private $charImage;

/**
 * @ORM\Column(type="integer", length=10)
 */
private $charStrength;

/**
 * @ORM\Column(type="integer", length=10)
 */
private $charDexterity;

/**
 * @ORM\Column(type="integer", length=10)
 */
private $charConstitution;

/**
 * @ORM\Column(type="integer", length=10)
 */
private $charInteligence;

/**
 * @ORM\Column(type="integer", length=10)
 */
private $charWisdom;

/**
 * @ORM\Column(type="integer", length=10)
 */
private $charCharisma;
/**
 * @return mixed
 */


 public function getCharId()
 {
    return $this->charId;
 }

/**
 * @return mixed
 */
 public function getCharTime()
 {
    return $this->charTime;
 }

/**
 * @param mixed $charTime
 */
 public function setCharTime($charTime): void
 {
    $this->charTime = $charTime;
 }


/**
 * @return mixed
 */
 public function getCharName()
 {
    return $this->charName;
 }

/**
 * @param mixed $charName
 */
 public function setCharName($charName): void
 {
    $this->charName = $charName;
 }

 /**
 * @return mixed
 */
 public function getCharImage()
 {
    return $this->charImage;
 }

 /**
 * @param mixed $charImage
 */
 public function setCharImage($charImage): void
 {
    $this->charImage = $charImage;
 }


/**
 * @return mixed
 */
 public function getCharRace()
 {
    return $this->charRace;
 }

/**
 * @param mixed $charRace
 */
 public function setCharRace($charRace): void
 {
    $this->charRace = $charRace;
 }

/**
 * @return mixed
 */
 public function getCharClass()
 {
    return $this->charClass;
 }

/**
 * @param mixed $charClass
 */
 public function setCharClass($charClass): void
 {
    $this->charClass = $charClass;
 }

 /**
 * @return mixed
 */
 public function getCharLevel()
 {
    return $this->charLevel; 
 }

/**
 * @param mixed $charLevel
 */
 public function setCharLevel($charLevel): void
 {
    $this->charLevel = $charLevel;
 }

 /**
 * @return mixed
 */
 public function getCharAlignment()
 {
    return $this->charAlignment;
 }

/**
 * @param mixed $charAlignment
 */
 public function setCharAlignment($charAlignment): void
 {
    $this->charAlignment = $charAlignment;
 }

 /**
 * @return mixed
 */
 public function getCharHitPoints()
 {
    return $this->charHitPoints;
 }

/**
 * @param mixed $charHitPoints
 */
 public function setCharHitPoints($charHitPoints): void
 {
    $this->charHitPoints = $charHitPoints;
 }

 /**
 * @return mixed
 */
 public function getCharExperience()
 {
    return $this->charExperience;
 }

/**
 * @param mixed $charExperience
 */
 public function setCharExperience($charExperience): void
 {
    $this->charExperience = $charExperience;
 }

 /**
 * @return mixed
 */
 public function getCharMagic()
 {
    return $this->charMagic;
 }

/**
 * @param mixed $charMagic
 */
 public function setCharMagic($charMagic): void
 {
    $this->charMagic = $charMagic;
 }

 /**
 * @return mixed
 */
 public function getCharHistory()
 {
    return $this->charHistory;
 }

/**
 * @param mixed $charHistory
 */
 public function setCharHistory($charHistory): void
 {
    $this->charHistory = $charHistory;
 }

 /**
 * @return mixed
 */
 public function getCharStrength()
 {
    return $this->charStrength;
 }

 /**
 * @param mixed $charStrength
 */
 public function setCharStrength($charStrength): void
 {
    $this->charStrength = $charStrength;
 }

/**
 * @return mixed
 */
 public function getCharDexterity()
 {
    return $this->charDexterity;
 }

 /**
 * @param mixed $charDexterity
 */
 public function setCharDexterity($charDexterity): void
 {
    $this->charDexterity = $charDexterity;
 }

/**
 * @return mixed
 */
 public function getCharConstitution()
 {
    return $this->charConstitution;
 }

 /**
 * @param mixed $charConstitution
 */
 public function setCharConstitution($charConstitution): void
 {
    $this->charConstitution = $charConstitution;
 }

/**
 * @return mixed
 */
 public function getCharInteligence()
 {
    return $this->charInteligence;
 }

 /**
 * @param mixed $charInteligence
 */
 public function setCharInteligence($charInteligence): void
 {
    $this->charInteligence = $charInteligence;
 }

/**
 * @return mixed
 */
 public function getCharWisdom()
 {
    return $this->charWisdom;
 }

 /**
 * @param mixed $charWisdom
 */
 public function setCharWisdom($charWisdom): void
 {
    $this->charWisdom = $charWisdom;
 }

/**
 * @return mixed
 */
 public function getCharCharisma()
 {
    return $this->charCharisma;
 }

 /**
 * @param mixed $charCharisma
 */
 public function setCharCharisma($charCharisma): void
 {
    $this->charCharisma = $charCharisma;
 }

}
